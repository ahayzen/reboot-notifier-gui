<!--
SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: MPL-2.0
-->

# Install

```
sudo apt install git-buildpackage
```

# Configure

Run the following command to create a branch for debian packaging, then add your debian/ folder and commit it.

```
git checkout -b debian/sid master
```

Example gbp config (we need network in pbuilder as we are using pip3)

```
[DEFAULT]
debian-branch = debian/sid
upstream-branch = master
upstream-tree = BRANCH
# upstream-vcs-tag = v%(version)s

[buildpackage]
dist = sid
export-dir = build
```

# Native Build

Once you have a tag in the origin/master and debian/changelog that matches this version,
a native package can be build with the following command.

Note this command needs to be run from the `debian/*` branch.

```
gbp buildpackage
```

# pbuilder Building

Install pbuilder and qemu (for cross builds).

```
sudo apt install pbuilder qemu-user-static
```

Configure pbuilder to find apt depends.

```
echo "PBUILDERSATISFYDEPENDSCMD="/usr/lib/pbuilder/pbuilder-satisfydepends-apt" >> ~/.pbuilderrc
```

## Cross build for Raspbian

Add Raspbian keyring

```
sudo mkdir -p /usr/local/share/keyrings
wget https://archive.raspberrypi.org/debian/raspberrypi.gpg.key qO- | sudo gpg --import --no-default-keyring --keyring /usr/local/share/keyrings/raspbian-archive-keyring.gpg
```

Create raspbian buster armhf pbuilder env.

```
DIST=buster ARCH=armhf git-pbuilder create --debootstrapopts --keyring=/usr/local/share/keyrings/raspbian-archive-keyring.gpg --mirror=https://archive.raspbian.org/raspbian
```

Build the package in pbuilder.

```
gbp buildpackage --git-arch=armhf --git-pbuilder
```

## Native pbuilder

Create debian buster amd64 pbuilder env.

```
DIST=buster ARCH=amd64 git-pbuilder create
```

Build the package in pbuilder.

```
gbp buildpackage --git-arch=amd64 --git-pbuilder
```
