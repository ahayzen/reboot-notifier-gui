<!--
SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: MPL-2.0
-->

A simple reboot notification gui that uses `org.freedesktop.Notifications` to present a popup to the user when `/var/run/reboot-required` exists.

# Installation

Build the debian package, see [README.debian.md](README.debian.md) for more info.

```
gbp buildpackage
```

Then install the resultant .deb file

```
sudo apt install ./reboot-notifier-gui_1.0.0-1_amd64.deb
```

Then enable the path watching for the users you want it.

```
systemctl --user enable --now reboot-notifier-gui.path
```

