#!/usr/bin/env python3

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
# SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

from dbus import Interface, SessionBus
from dbus.exceptions import DBusException
from gi.repository import GLib
import notify2
from os import path, system
from psutil import process_iter
import subprocess
from time import sleep

MAIN_LOOP = GLib.MainLoop()


class RebootNotification(object):
    def __init__(self):
        self.current_notification = None

    def on_action(self, notification, action):
        if action == "reboot":
            self.on_reboot()
        elif action == "systemd-reboot":
            system("systemctl reboot")
        else:
            self.on_closed(notification, action)

    def on_closed(self, notification, action=None):
        if notification.id == self.current_notification:
            print("Reboot required notification closed")
            MAIN_LOOP.quit()

    def on_reboot(self):
        # Try to request to shutdown using GNOME's system dialogs
        # if we can't then fallback to calling system
        try:
            session = SessionBus()
            proxy = session.get_object("org.gnome.SessionManager",
                                       "/org/gnome/SessionManager")
            interface = Interface(
                proxy, dbus_interface="org.gnome.SessionManager")
        except DBusException:
            self.show_notification(
                summary="A reboot is required",
                message="Are you sure you want to reboot now?",
                actions=[("no", "No"), ("systemd-reboot", "Yes")])
        else:
            # If the user selects cancel we will get an exception
            try:
                interface.Reboot()
            except DBusException:
                pass

    def show_notification(self, summary, message, actions):
        # Create a reboot required notification
        notification = notify2.Notification(
            summary=summary,
            message=message,
            icon="system-reboot",
        )

        # Add actions to the notification
        for action, name in actions:
            notification.add_action(action, name, self.on_action)

        notification.connect("closed", self.on_closed)
        # Set the urgency to critical so that it is a persistent notification
        notification.set_urgency(notify2.URGENCY_CRITICAL)
        # Show the notification
        notification.show()
        # Update current notification
        self.current_notification = notification.id


def is_apt_daily_upgrade_running():
    # If the is-active state of apt-daily-upgrade is not inactive
    # then it is likely activating or active
    return subprocess.run(
        ["systemctl", "is-active", "apt-daily-upgrade"],
        stdout=subprocess.PIPE).stdout.decode("utf-8").strip() != "inactive"


def is_dpkg_locked():
    # We can't check dpkg lock as we are a user, so look for process name
    return len([
        proc
        for proc in process_iter() if proc.name() in ("apt", "apt-get", "dpkg")
    ]) > 0


if __name__ == "__main__":
    # Check that the reboot-required path exists
    if path.exists("/var/run/reboot-required"):
        # Ensure that apt-daily-upgrade has finished
        # Also helps if unattended upgrades is doing small chunks
        #
        # Wait until apt has finished before we show the notification
        # in-case we have come from manual update
        while is_apt_daily_upgrade_running() or is_dpkg_locked():
            print("Waiting for apt-daily-upgrade or dpkg lock to finish")
            sleep(1)

        # Init notify2 with glib
        notify2.init("com.ahayzen.reboot-notifier-gui", mainloop="glib")

        # show notification
        reboot = RebootNotification()
        reboot.show_notification(
            summary="A reboot is required",
            message="Updates have been installed, "
            "please restart your computer to complete them.",
            actions=[("not-now", "Not Now"), ("reboot", "Reboot")])

        # Run GLib main loop so that we can receive callbacks
        MAIN_LOOP.run()
